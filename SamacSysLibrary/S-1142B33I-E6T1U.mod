PCBNEW-LibModule-V1  2020-03-29 03:03:13
# encoding utf-8
Units mm
$INDEX
S1142B33IE6T1U
$EndINDEX
$MODULE S1142B33IE6T1U
Po 0 0 0 15 5e8001e1 00000000 ~~
Li S1142B33IE6T1U
Cd S-1142B33I-E6T1U
Kw Integrated Circuit
Sc 0
At SMD
AR 
Op 0 0 0
T0 0.000 -0 1.27 1.27 0 0.254 N V 21 N "IC**"
T1 0.000 -0 1.27 1.27 0 0.254 N I 21 N "S1142B33IE6T1U"
DS -1.95 -2.51 1.95 -2.51 0.2 24
DS 1.95 -2.51 1.95 2.51 0.2 24
DS 1.95 2.51 -1.95 2.51 0.2 24
DS -1.95 2.51 -1.95 -2.51 0.2 24
DS -3.29 -4.275 3.29 -4.275 0.1 24
DS 3.29 -4.275 3.29 4.275 0.1 24
DS 3.29 4.275 -3.29 4.275 0.1 24
DS -3.29 4.275 -3.29 -4.275 0.1 24
DS -1.95 -1.775 -1.95 1.775 0.1 21
DS 1.95 -1.775 1.95 1.775 0.1 21
DS -2.8 2.6 -2.8 2.6 0.2 21
DS -2.8 2.4 -2.8 2.4 0.2 21
DA -2.8 2.5 -2.800 2.6 1800 0.2 21
DA -2.8 2.5 -2.800 2.4 1800 0.2 21
$PAD
Po -1.910 2.64
Sh "1" R 0.760 1.270 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 0.000 2.64
Sh "2" R 1.270 2.030 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 1.910 2.64
Sh "3" R 0.760 1.270 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 1.910 -2.64
Sh "4" R 0.760 1.270 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 0.000 -2.64
Sh "5" R 1.270 2.030 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po -1.910 -2.64
Sh "6" R 0.760 1.270 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$EndMODULE S1142B33IE6T1U
$EndLIBRARY
